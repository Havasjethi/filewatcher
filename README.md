# FileWatcher

Watch a specific file for changes.
After the Os sends a Notification signal about the file change run the input command with `sh`.

## Example usage:

```
# file_watcher <options> <command>

file_watcher --file=some.ext "echo The file has been updated"

file_watcher --folder=src "echo This file ':FileName' has been updated"
```

## Command Wild card

It is possible to give dinamyc parameters to the called program by adding variable


| Parameter | Action |
| --- | ---| 
| :File | Absolute Path of the file |
| :FileName | Name of the file (with extentsion) |
| :Folder | The folder containing the file |
