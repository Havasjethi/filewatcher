use clap::Parser;
use notify::{Watcher, RecursiveMode,  watcher, DebouncedEvent };
use std::path::PathBuf;
use std::time::Duration;
use std::sync::mpsc::channel;

#[derive(Parser)]
#[clap(name = "FileWatch")]
#[clap(author = "Jozsef Kulcsar <havasjethi@gmail.com>")]
#[clap(version = "0.2.0")]
#[clap(about = "Watch changes in your fileSysteam, and react to it!", long_about = None)]
struct Cli {
    /// File to watch
    #[clap(long = "file")]
    files: Option<Vec<String>>,
    
    /// Command to exectute upon change
    command: String,

    /// Folders to watch recursively
    /// Use -f f_1 -f f_2 To watch multiple folders
    #[clap(short, long = "folder")]
    folders: Option<Vec<String>>,

    #[clap(long = "no-dyn")]
    no_dyn_vars: bool,
}

fn main() {
    let cli = Cli::parse();

    let (tx, rx) = channel();
    let debounce_time = Duration::from_millis(50);
    let mut watcher_count = 0;
    let mut watchers = Vec::new();

    let create_watcher = |entity: &String, mode: RecursiveMode, watchers: &mut Vec<notify::INotifyWatcher>| {
        let mut w = watcher(tx.clone(), debounce_time.clone())
            .expect("Unable to create watcher");

        w
            .watch(entity, mode)
            .expect(&format!("Unable to watch enity: {}", entity));

        watchers.push(w)
    };


    if let Some(folders) = &cli.folders {
        for folder in folders {
            create_watcher(&folder, RecursiveMode::Recursive, &mut watchers);
            watcher_count += 1;
        }
    }

    if let Some(file_names) = &cli.files {
        for file in file_names {
            create_watcher(&file, RecursiveMode::NonRecursive, &mut watchers);
            watcher_count += 1;
        }
    }
    
    if watcher_count == 0 {
        panic!("No entities given to watch");
    }


    let execute_command : Box<dyn Fn(PathBuf) -> std::process::Output> = if cli.no_dyn_vars  {
        Box::from(|_: PathBuf| execute_command(&cli.command.to_owned().clone()))
    } else { 
        Box::from(
            |p: PathBuf| execute_command(
                &cli.command.clone()
                    .replace(":FileName", p.file_name().expect("File Name not exists?!?").to_str().expect("what"))
                    .replace(":File", p.to_str().expect("Str not found for PathBuf"))
                    .replace(":Folder", p.parent().expect("Parent not exists").to_str().expect(":Folder Cannot be created"))
            )
        )
    };

    loop {
        match rx.recv() {
           Ok(event) => {
            match event {
                DebouncedEvent::Write(path_buf) => {
                    let result = execute_command(path_buf);
                    write_result(result);
                },
                _ => () // println!("Other event, but we only care about writes {:?}", event)
            }
           },
           Err(e) => println!("watch error: {:?}", e),
        }
    }

}

fn execute_command (command: &String) -> std::process::Output {
    create_platform_spec_command()
        .arg("-c")
        .arg(command)
        .output()
        .expect("failed to execute process")
}

fn create_platform_spec_command () -> std::process::Command {
    // TODO :: Add Cross plaform capabilities
    std::process::Command::new("sh")
}

fn write_result (result: std::process::Output) {
    let _stdout = parse_str(result.stdout);
    let _stderr = parse_str(result.stderr);
    println!("{}", _stdout);
    println!("{}", _stderr);
}

fn parse_str(x: Vec<u8>) -> String {
    let might_be = String::from_utf8(x.clone());
    
    if might_be.is_ok() {
        return might_be.expect("msg");
    }
    let inner = x.into_boxed_slice();
    let might_be = String::from_utf8_lossy(&inner);
    might_be.to_string()
}
